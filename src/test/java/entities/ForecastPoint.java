package entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.util.List;

@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ForecastPoint {
    private Long altitudeBottom;
    private Long altitudeTop;
    private List<Object> columnLabels;
    private List<Object> columnNames;
    private String createDate;
    private String description;
    private Double east;
    private String endDate;
    private List<Object> extraColumns;
    private String icon;
    private String id;
    private Boolean isGroup;
    private List<Object> metadata;
    private String name;
    private Double north;
    private String parent;
    private List<Object> services;
    private Double south;
    private String startDate;
    private Type type;
    private String user;
    private Double west;
}

package core;

import lombok.extern.log4j.Log4j2;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

@Log4j2
public class PropertiesResourceManager {
    private static final String PROPERTIES_PATH = "target/classes/test.properties";
    String Url;
    private Properties properties = new Properties();

    public PropertiesResourceManager() {
        try {
            properties.load(new FileInputStream(PROPERTIES_PATH));
            initializeProperties();
        } catch (IOException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public String getProperty(final String key) {
        return properties.getProperty(key);
    }

    public Properties getAllProperties() {
        return properties;
    }

    private void initializeProperties() {
        Url = properties.getProperty("URL");
    }
}

Feature: Get Forecast Points

  Scenario Template: Validate User Gets Forecast Points
    Given On Get Request for getForecastPoints endpoint with "<URL>"
    Then Validate Response Status Code Equals "<status>"
    Examples:
      | URL            | status |
      | language/en-Gb | 200    |
      | language/      | 404    |
      | language/fr-Fr | 200    |
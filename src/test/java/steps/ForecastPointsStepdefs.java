package steps;

import implementation.ForecastPointServiceImpl;
import io.cucumber.java.en.*;
import io.restassured.response.ValidatableResponse;
import org.junit.Assert;
import services.IForecastPointService;

import static scenarioContext.RunContext.RUN_CONTEXT;

public class ForecastPointsStepdefs {
    IForecastPointService forecastPointService = new ForecastPointServiceImpl();

    @Given("On Get Request for getForecastPoints endpoint with {string}")
    public void onGetRequestForGetForecastPointsEndpointWith(String url) {
        forecastPointService.getForecastPoints(url);
    }

    @Then("Validate Response Status Code Equals {string}")
    public void validateResponseStatusCodeEquals(String status) {
        ValidatableResponse response = RUN_CONTEXT.get("response", ValidatableResponse.class);
        int actualStatusCode = response.extract().statusCode();
        int expectedStatusCode = Integer.parseInt(status);
        Assert.assertEquals(actualStatusCode, expectedStatusCode);
    }
}

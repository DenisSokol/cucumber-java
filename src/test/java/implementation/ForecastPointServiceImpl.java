package implementation;

import core.PropertiesResourceManager;
import entities.ForecastPoint;
import io.restassured.response.ValidatableResponse;
import lombok.extern.log4j.Log4j2;
import services.IForecastPointService;

import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;
import static scenarioContext.RunContext.RUN_CONTEXT;

@Log4j2
public class ForecastPointServiceImpl implements IForecastPointService {
    PropertiesResourceManager propertiesResourceManager = new PropertiesResourceManager();

    @Override
    public List<ForecastPoint> getForecastPoints(String url) {
        String URL = propertiesResourceManager.getProperty("URL") + url;

        List<ForecastPoint> forecastPoints = null;

        ValidatableResponse response = given().log().everything()
                .get(URL)
                .then().log().ifError();

        RUN_CONTEXT.put("response", response);

        try {
            forecastPoints = response.extract().jsonPath().getList("data.", ForecastPoint.class);
        } catch (Exception e) {
            log.error("ForecastPoint request exception: " + Arrays.toString(e.getStackTrace()));
        }

        return forecastPoints;
    }
}

package services;

import entities.ForecastPoint;
import java.util.List;

public interface IForecastPointService {
    List<ForecastPoint> getForecastPoints(String url);
}

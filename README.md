## Stack 
1. Java 11
2. Cucumber/Gherkin
3. Allure Reports

# How to run tests:
 	- To run tests use the following command: mvn clean install

# How to add tests:
    1. Inside of "Features" folder in Tests project create new feature file and describe scenario there, or add required steps in existing feature file 
    2. Right-click inside the feature file and left-click "Generate Step Definitions" 
    3. Save new Java class in required place inside of "Steps" package in src/test/java folder 
    4. Run the command: mvn clean install. You can find more detailed instructions here "https://cucumber.io/"

# Reporting: 
 - Allure report. Once the test execution is over navigate to target/site/allure-maven-plugin/index.html to see the report

